#ifndef __SPHERE_h
#define __SPHERE_h

#include <iostream>
#include <cmath>
#include "Vec3f.h"
#include "GeometricObjects.h"

using std::cout;
using std::endl;
using std::ios;

class Sphere {
   public:
      Sphere (float x, float y, float z, float squared_radius, float size, float material) : materialId (material), size_ (size),
      radius_squared (squared_radius), pos (x,y,z) {}
      inline bool intersect (Vec3f& camera_dir, Vec3f& camera_ori, Sphere &s, float &t, int x, int y) const;
      inline bool generateShadows (Vec3f & camera_dir, Vec3f& camera_ori, Sphere &s, int x, int y) const;
      inline Vec3f getNormal (Vec3f &intersection, int x, int y) const;
      int getMaterialId () const { return materialId; }
      float getSquaredRadius () const {return radius_squared;}
      float getRadius () const {return size_;}
      inline Vec3f& getCenter () {return pos;}
    private:
      Vec3f pos;
      float radius_squared;
      float size_;
      int materialId;
};

inline Vec3f Sphere::getNormal (Vec3f &intersection, int x, int y) const {
   Vec3f n  (intersection - pos);
   n.normalize ();
   return n;
}

/*bool Sphere::hitSphere(const Ray &r, const Sphere &s, float &t) {
}*/

inline bool Sphere::generateShadows (Vec3f& camera_dir, Vec3f& camera_ori, Sphere &s, int x, int y) const {
   Vec3f hipt (s.getCenter() - camera_ori);
   float cat_adj_lgt = camera_dir * hipt;
   if (cat_adj_lgt<0.0f)
      return false;
   float cat_opst_sqrd_lgt =  hipt * hipt - cat_adj_lgt * cat_adj_lgt;
   if (cat_opst_sqrd_lgt>s.radius_squared)
      return false;
   return true;
}

inline bool Sphere::intersect(Vec3f& camera_dir, Vec3f& camera_ori, Sphere &s, float &t, int x, int y) const {
   Vec3f hipt (s.getCenter() - camera_ori);
   float cat_adj_lgt = camera_dir * hipt;
   if (cat_adj_lgt<0.0f)
      return false;
   float cat_opst_sqrd_lgt =  hipt * hipt - cat_adj_lgt * cat_adj_lgt;
   if (cat_opst_sqrd_lgt>s.radius_squared)
      return false;
   float cat_adj_smaller_sqrd_lgt = s.radius_squared - cat_opst_sqrd_lgt;
   float t0 = cat_adj_lgt - sqrt (cat_adj_smaller_sqrd_lgt);
   bool ret_value = false;
   if ((t0 > 0.1f) && (t0 < t)) {
      t = t0;
      ret_value = true;
   }
   return ret_value;

   //float t1 = cat_adj_lgt + sqrt (cat_adj_smaller_sqrd_lgt);
   /*if (t0<t1 && t0>0.1f) {
      t=t0;
      return true;
   }
   else if (t1<t0 && t1>0.1f) {
      t=t1;
      return true;
   }*/
   //ScratchPixel code for comparison
   /*if (t0 > t1) std::swap(t0, t1);
   if (t1 < 0) {
      t1 = t0; // if t0 is negative, let's use t1 instead
      if (t0 < 0) return false; // both t0 and t1 are negative
   }
   t = t0;
   return true;*/
}


#endif
