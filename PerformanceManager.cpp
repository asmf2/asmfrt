#include "PerformanceManager.h"

using std::cout;
using std::endl;

void PerformanceManager::start () {
    start_var = clock();
}

void PerformanceManager::end () {
    end_var = clock();
}

void PerformanceManager::print () {
    cout << (end_var-start_var)/CLOCKS_PER_SEC << endl;
}
