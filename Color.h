class Color {
   public:
      inline Color& operator += (const Color& c) {
         //cout << "c: " << c.red << ' ' << c.green << ' ' << c.blue << endl;
         //cout << "this: " << this->red << ' ' << this->green << ' ' << this->blue << endl;
         this->red = this->red + c.red;
         this->green = this->green + c.green;
         this->blue = this->blue + c.blue;
         return *this;
      }
      float red,green,blue;
};

inline Color operator / (const Color& c, float a) {
   Color color;
   color.red = c.red / a;
   color.green = c.green / a;
   color.blue = c.blue / a;
   return color;
}

inline Color operator * (const Color& c, float a) {
   Color color;
   color.red = c.red * a;
   color.green = c.green * a;
   color.blue = c.blue * a;
   return color;
}

inline Color operator * (float a, const Color& c) {
   Color color;
   color.red = c.red * a;
   color.green = c.green * a;
   color.blue = c.blue * a;
   return color;
}

inline Color operator * (const Color& c, const Color &d) {
   Color color;
   //cout << "c :" << c.red << ' ' << c.green << ' ' << c.blue << endl;
   //cout << "d :" << d.red << ' ' << d.green << ' ' << d.blue << endl;
   color.red = c.red * d.red;
   color.green = c.green * d.green;
   color.blue = c.blue * d.blue;
   //cout << "color :" << color.red << ' ' << color.green << ' ' << color.blue << endl;
   return color;
}

inline Color& operator + (const Color& c, const Color &d) {
   Color color;
   color.red = c.red + d.red;
   color.green = c.green + d.green;
   color.blue = c.blue + d.blue;
   return color;
}

