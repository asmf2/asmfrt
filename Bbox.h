#include <vector>
#include <algorithm>
#include "Triangle.h"

using std::vector;
using std::swap;

class Bbox {
   public:
      Bbox (float min_x,float min_y,float min_z,float max_x,float max_y,float max_z,int min_triangle,int max_triangle, vector <Triangle>& tri_ref): min_X (min_x), min_Y (min_y), min_Z (min_z)
, max_X (max_x), max_Y (max_y), max_Z (max_z), min_tri (min_triangle), max_tri (max_triangle), tri_vec_ref (tri_ref) {}
         bool intersect (Vec3f& camera_dir, Vec3f& camera_ori);
         inline int getFirstTri () {return min_tri;}
         inline int getLastTri () {return max_tri;}
         Bbox& operator = (Bbox& tmp) {
            tri_vec_ref = tmp.tri_vec_ref;
            return *this;
         }
   private:
      float min_X,min_Y,min_Z,max_X,max_Y,max_Z;
      int min_tri, max_tri;
      vector <Triangle>& tri_vec_ref;
};

inline bool Bbox::intersect (Vec3f& camera_dir, Vec3f& camera_ori) {
   float tmin = (min_X - camera_ori.x) / camera_dir.x;
   float tmax = (max_X - camera_ori.x) / camera_dir.x;
   //(tmin > tmax) ? tmin : tmax;
   if (tmin > tmax) swap(tmin, tmax);

   float tymin = (min_Y - camera_ori.y) / camera_dir.y;
   float tymax = (max_Y - camera_ori.y) / camera_dir.y;

   //(tymin > tymax) ? tymin : tymax;
   if (tymin > tymax) swap(tymin, tymax);

   if ((tmin > tymax) || (tymin > tmax))
      return false;
   if (tymin > tmin)
      tmin = tymin;
   if (tymax < tmax)
      tmax = tymax;

   float tzmin = (min_Z - camera_ori.z) / camera_dir.z;
   float tzmax = (max_Z - camera_ori.z) / camera_dir.z;

   //(tzmin > tzmax) ? tzmin : tzmax;
    if (tzmin > tzmax) swap(tzmin, tzmax);

   if ((tmin > tzmax) || (tzmin > tmax))
      return false;
   if (tzmin > tmin)
      tmin = tzmin;
   if (tzmax < tmax)
      tmax = tzmax;
   return true;
}
