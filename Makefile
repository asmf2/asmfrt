CFLAGS=-I. -O2 -fexpensive-optimizations --fast-math -march=native
DEPS = PerformanceManager.h InputHandler.h Vec3f.h Image.h FileManager.h GeometricObjects.h TGAManager.h Plane.h Sphere.h Triangle.h Scene.h TextManager.h RayTracer.h Bbox.h
OBJ = PerformanceManager.o  InputHandler.o Image.o FileManager.o TGAManager.o Plane.o Scene.o TextManager.o RayTracer.o main.o Bbox.o

%.o: %.cpp $(DEPS)
	g++ -c -o $@ $< $(CFLAGS)

RayTracer: $(OBJ) 
	g++ -lpthread -o $@ $^ $(CFLAGS)

clean:
	rm -rf *.o
