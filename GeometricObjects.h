#ifndef __GEOMETRICOBJECTS_H
#define __GEOMETRICOBJECTS_H
#include <iostream>
#include "Vec3f.h"
#include "Color.h"

using std::cout;
using std::endl;

struct Light {
   Vec3f pos;
   Color color;
   //int intensity;
};

struct Material {
   Color color;
   float reflection;
   float ior;
   float reverseIor;
};

struct Mtl {
   float ns;
   float ni;
   float d;
   float tr;
   Vec3f tf;
   int illum;
   Color ka;
   Color kd;
   Color ks;
   Color ke;
   float reflection;
   std::string material_name;
};

#endif
