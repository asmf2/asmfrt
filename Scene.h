#ifndef SCENE_H_INCLUDED
#define SCENE_H_INCLUDED
#include <vector>
#include <string>
#include <cstdlib>
#include "GeometricObjects.h"
#include "Sphere.h"
#include "Plane.h"
#include "Triangle.h"
#include "Bbox.h"

using std::vector;
using std::string;

class Scene {
   public:
      Scene (int, int);
      inline vector <Sphere>& getSphereContainer() { return sphere_container; }
      inline vector <Plane>& getPlaneContainer() { return plane_container; }
      inline vector <Triangle>& getTriangleContainer() { return triangle_container; }
      inline vector <Material>& getMaterialContainer() { return material_container; }
      inline vector <Light>& getLightContainer() { return light_container; }
      inline vector <Mtl>& getMtlContainer() { return mtl_material_container; }
      inline vector <Bbox>& getBboxContainer () { return Bbox_container; }
      void setCamera (float (&) [100]);
      void setMaterials (float (&) [100]);
      void setLights (float (&) [100]);
      void setSpheres (float (&) [100]);
      void setPlanes (float (&) [100]);
      void setTriangles (float (&) [100]);
      void insertMtlData (Mtl&, string&, float (&) [3]);
      void insertMtlMaterial (string& mat_name, Mtl mtl) {mtl.material_name = mat_name; mtl_material_container.push_back (mtl);}
      void assembleTriangles (vector <Vec3f>&, vector <int>&, string&, string&);
      Vec3f getCamera_ori () { return camera_ori; }
      Vec3f getCamera_dir () { return camera_dir; }
      float getCameraDirZ () { return camera_dir.getZ(); }
      const float getFov () { return fov; }
      void toWindCW ();
      void copyMaterials ();
      void setTriMem () { triangle_array = new Triangle [triangle_container.size()]; };
      void copyTriangles () { for (unsigned int i=0; i<triangle_container.size(); i++) triangle_array [i] = triangle_container[i]; }
      void unsetTriMem () { delete triangle_array; }
      Triangle* getTriArray () { return triangle_array; }
      inline const int getNTriangles () { return triangle_count; }
      //void setNTriangles () { triangle_count = triangle_container.size(); }
      void generateBBox ();
   private:
      int triangle_count;
      int spheres_count;
      //int current_triangle;
      Triangle* triangle_array;
      float fov;
      Vec3f camera_ori;
      Vec3f camera_dir;
      std::vector <Material> material_container;
      std::vector <Sphere> sphere_container;
      std::vector <Plane> plane_container;
      std::vector <Triangle> triangle_container;
      std::vector <Light> light_container;
      std::vector <Mtl> mtl_material_container;
      std::vector <Bbox> Bbox_container;
};

#endif // SCENE_H_INCLUDED
