#ifndef __PERFORMANCEMANAGER_h
#define __PERFORMANCEMANAGER_h

#include <iostream>

class PerformanceManager {
    public:
        void start ();
        void end   ();
        void print ();
    private:
        double start_var;
        double end_var;
};

#endif
