#include "Image.h"

void Image::insertData (int width, int height, int bpp) {
    m_width = width;
    //cout << "Width " << m_width << endl;
    m_height = height;
    //cout << "Height " << m_height << endl;
    m_bitsPerPixel = bpp;
    //cout << "bitsPerPixel " << m_bitsPerPixel << endl;
    m_bytesPerPixel = bpp >> 3;
    m_size = m_width*m_height*m_bytesPerPixel;
}
