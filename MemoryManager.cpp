#include <iostream>
#include "MemoryManager.h"

using std::ios;
using std::cout;
using std::endl;

bool MemoryManager::setTextureMemory (const Image &image) {
    textureStorage = new char [image.getSize()];
    if (textureStorage == NULL) {
      cout << "Couldn't allocate memory to store the TGA file in memory. Please run the program again: " << endl;
      return false;
    }
    else
      return true;
}

char * MemoryManager::getMemoryAdress () {
   return textureStorage;
}

void MemoryManager::freeMemory () {
  delete [] textureStorage;
}
