#include "Plane.h"

Plane::Plane (float a, float b, float c, float d, int id) {
   normal.x = a, normal.y = b, normal.z = c, dist = d, materialId = id;
}

bool Plane::intersect (Vec3f &camera_dir, Vec3f &camera_ori, const Plane &p, float &t, int x, int y)  {
   bool retvalue = false;
   float vd = normal * camera_dir;
   if (vd==0.00f)
      return retvalue;
   float intersection = - ((normal * camera_ori) + dist) / (vd);
   if (0.1f<intersection && intersection<t) {
      t=intersection;
      retvalue = true;
   }
   if (vd>0) {
      //normal.x = -normal.x; normal.y = -normal.y; normal.z = -normal.z;
      //normal2.invert ();
      -normal;
   }
   return retvalue;
}
