#include "Scene.h"

Scene::Scene (int tri_ctr, int spheres_ctr) {
   triangle_count=0;
   spheres_count=0;
   triangle_array=NULL;
}

void Scene::toWindCW () {
   //cout << "Scene::toWindCW:" << endl;
   //int count=0;
   for (int i=0; i<triangle_count; i++) {
      //Triangle &t = triangle_container[i];
      Triangle &t = triangle_array [i];
      Vec3f v1,v2,v3;
      v1 = t.getVert1(), v2 = t.getVert2_edge1(), v3 = t.getVert3_edge2();
      Vec3f edge1 = v2 - v1;
      Vec3f edge2 = v3 - v1;
      Vec3f normal;
      normal.cross (edge1, edge2);
      if (normal.getZ()>0) {
         //count++;
         t.setVert2_edge1 (v3);
         t.setVert3_edge2 (v2);
         -normal;
      }
   normal.normalize ();
   t.setNormal (normal);
   t.setVert2_edge1 (t.getVert2_edge1() - t.getVert1());
   t.setVert3_edge2 (t.getVert3_edge2() - t.getVert1());
   }
}

void Scene::setCamera (float (&lineNumbers) [100]) {
	camera_ori.setX (lineNumbers[0]), camera_ori.setY(lineNumbers[1]), camera_ori.setZ(lineNumbers[2]);
	camera_dir.setX (lineNumbers[3]), camera_dir.setY(lineNumbers[4]), camera_dir.setZ(lineNumbers[5]);
	fov = lineNumbers[6];
}

void Scene::setMaterials (float (&lineNumbers) [100]) {
	//cout << "Scene::setMaterial" << endl;
	Material material;
	material.color.red = lineNumbers[0], material.color.green = lineNumbers[1], material.color.blue = lineNumbers[2];
	material.reflection = lineNumbers[3];
	material.ior = lineNumbers[4];
  	material.reverseIor = lineNumbers[5];
	material_container.push_back (material);
	//cout << "material: " << material.color.red << ' '  << material.color.green << ' '  << material.color.blue << ' ' <<
	//material.reflection << ' ' << material.ior << ' ' << material.reverseIor << endl;
}

void Scene::setLights (float (&lineNumbers) [100]) {
	//cout << "Scene::setLights" << endl;
	Light light;
	light.pos.setX (lineNumbers[0]), light.pos.setY (lineNumbers[1]), light.pos.setZ (lineNumbers[2]);
	light.color.red = lineNumbers[3]*lineNumbers[6], light.color.green = lineNumbers[4]*lineNumbers[6],
	light.color.blue = lineNumbers[5]*lineNumbers[6];
	//cout << "light.color.red, green,blue: " << light.color.red << ' ' << light.color.green << ' ' << light.color.blue << endl;
	light_container.push_back (light);
	//exit (-1);
}

void Scene::setSpheres (float (&lineNumbers) [100]) {
	//cout << "Scene::setSpheres" << endl;
	Sphere sphere (lineNumbers[0], lineNumbers[1], lineNumbers[2] ,lineNumbers[3], lineNumbers[4], lineNumbers[5]);
	sphere_container.push_back (sphere);
	/*cout << "lineNumbers[0], lineNumbers[1] and lineNumbers[2]: " << lineNumbers[0] << ' ' << lineNumbers[1] << ' ' << lineNumbers[2] << endl;
	cout << "sphere.getMaterialID(): " << sphere.getMaterialId () << endl;
	cout << "sphere.getRadius(): " << sphere.getRadius() << endl;
	cout << "sphere.getSquaredRadius(): " << sphere.getSquaredRadius() << endl;
	cout << "sphere pos (vec): " << sphere.pos.x << ' ' << sphere.pos.y << ' ' << sphere.pos.z << endl;
	cout << "" << endl;*/
	//exit (-1);
}

void Scene::setPlanes (float (&lineNumbers) [100]) {
	//cout << "Scene::setPlanes" << endl;
	Plane plane (lineNumbers[0],lineNumbers[1],lineNumbers[2],lineNumbers[3],lineNumbers[4]);
	//plane.setNormal2 (lineNumbers[0],lineNumbers[1],lineNumbers[2]);
	plane_container.push_back (plane);
	//cout << "plane: " << planeContainer[0].getNormalX() << planeContainer[0].getNormalY() << planeContainer[0].getNormalZ() <<endl;
}

void Scene::setTriangles (float (&lineNumbers) [100]) {
   //cout << "setTriangles" << endl;
	Vec3f tmp  (lineNumbers[0],lineNumbers[1],lineNumbers[2]);
	Vec3f tmp2 (lineNumbers[3],lineNumbers[4],lineNumbers[5]);
	Vec3f tmp3 (lineNumbers[6],lineNumbers[7],lineNumbers[8]);
   //Triangle triangle (tmp,tmp2,tmp3, lineNumbers[9]);
	Triangle triangle (tmp,tmp2,tmp3,lineNumbers[9]);

	triangle_container.push_back (triangle);
	/*cout << "vert11 " << triangle.vert11.getX() << ' ' << triangle.vert11.getY() << ' ' << triangle.vert11.getZ() << ' ' << endl;
	cout << "vert22 " << triangle.vert22.getX() << ' ' << triangle.vert22.getY() << ' ' << triangle.vert22.getZ() << ' ' << endl;
	cout << "vert33 " << triangle.vert33.getX() << ' ' << triangle.vert33.getY() << ' ' << triangle.vert33.getZ() << ' ' << endl;*/
}

void Scene::insertMtlData (Mtl& mtl, string& line, float (&line_numbers) [3]) {
   //cout << "Scene::insertMtlData" << endl;
   //cout << line[0] << ' ' << line[1] << endl;
   if (line[0] == 'N' && line[1] == 's') {
      //cout << "Ns" << endl;
      mtl.ns = line_numbers[0];
      //cout << "mtl.ns: " << mtl.ns << endl;
   }
   else if (line[0] == 'N' && line[1] == 'i') {
      //cout << "Ni" << endl;
      mtl.ni = line_numbers [0];
      //cout << "mtl.ni: " << mtl.ni << endl;
   }
   else if (line[0] == 'd') {
      //cout << "d" << endl;
      mtl.d = line_numbers [0];
      //cout << "mtl.d: " << mtl.d << endl;
   }
   else if (line[0] == 'T' && line[1] == 'r') {
      //cout << "Tr" << endl;
      mtl.tr = line_numbers[0];
      //cout << "mtl.tr: " << mtl.tr << endl;
   }
   else if (line[0] == 'T' && line[1] == 'f') {
      //cout << "Tf" << endl;
      mtl.tf.setX (line_numbers [0]), mtl.tf.setY (line_numbers [1]), mtl.tf.setZ (line_numbers [2]);
      //cout << "mtl.tf: " << mtl.tf.getX () << ' ' << mtl.tf.getY () << ' ' << mtl.tf.getZ () << endl;
   }
   else if (line[0] == 'i' && line[1] == 'l') {
      //cout << "illum" << endl;
      mtl.illum = line_numbers [0];
      //cout << "mtl.illum: " << mtl.illum << endl;
   }
   else if (line[0] == 'K' && line[1] == 'a') {
      //cout << "Ka" << endl;
      mtl.ka.red = line_numbers [0], mtl.ka.green = line_numbers [1], mtl.ka.blue = line_numbers [2];
      //cout << "mtl.ka: " << mtl.ka.red << ' ' << mtl.ka.green << ' ' << mtl.ka.blue << endl;
   }
   else if (line[0] == 'K' && line[1] == 'd') {
      //cout << "Kd" << endl;
      //cout << "line_numbers [0] " << line_numbers [0] << ' ' << "line_numbers [1]: " << line_numbers [1] <<  endl;
      mtl.kd.red = line_numbers [0], mtl.kd.green = line_numbers [1], mtl.kd.blue = line_numbers [2];
      //cout << "mtl.kd: " << mtl.kd.red << ' ' << mtl.kd.green << ' ' << mtl.kd.blue << endl;
   }
   else if (line[0] == 'K' && line[1] == 's') {
      //cout << "Ks" << endl;
      mtl.ks.red = line_numbers [0], mtl.ks.green = line_numbers [1], mtl.ks.blue = line_numbers [2];
      //cout << "mtl.ks: " << mtl.ks.red << ' ' << mtl.ks.green << ' ' << mtl.ks.blue << endl;
   }
   else if (line[0] == 'K' && line[1] == 'e') {
      //cout << "Ke" << endl;
      mtl.ke.red = line_numbers [0], mtl.ke.green = line_numbers [1], mtl.ke.blue = line_numbers [2];
      //cout << "mtl.ke: " << mtl.ke.red << ' ' << mtl.ke.green << ' ' << mtl.ke.blue << endl;
   }
   mtl.reflection = 0.5;
   //cout << "" << endl;
}

void Scene::assembleTriangles (vector <Vec3f>& vertices, vector <int>& faces, string& mat_name, string& file_name) {
   //cout << "Scene::assembleTriangles" << endl;
   //cout << "triangles_count: " << triangle_count << endl;
   int color;
   int number=0;
   //cout << "mat_name: " <<  mat_name << endl;
   if (file_name == "IronMan.obj" ) {
      if (mat_name != "gold" && mat_name != "yellow" && mat_name != "red" && mat_name != "black" && mat_name != "silver" && mat_name != "darksilver" && mat_name != "lambert1" && "14_-_Default")
      return;
   }
   for (int i=0; i<mtl_material_container.size(); i++)
      if (mat_name==mtl_material_container[i].material_name)
         color=i;
   Vec3f tmp [faces.size()];
	for (int i=0; i<faces.size(); i++)
      tmp [i] = vertices [faces[i] -1];
   for (int i=2; i<faces.size(); i++) {
      //Triangle triangle (tmp[0],tmp[i-1],tmp[i],color);
      //cout << "Vertices: " << tmp[0].getX() << ' ' << tmp[i-1].getX() << ' ' << tmp [i].getZ() << endl;
      //cout << "Edge: " << edge.getX() << ' ' << edge.getX() << ' ' << edge.getZ() << endl;
      //cout << "Edge2: " << edge2.getX() << ' ' << edge2.getX() << ' ' << edge2.getZ() << endl;
      Triangle triangle (tmp[0],tmp[i-1],tmp[i],color);
      triangle_container.push_back (triangle);

      //cout << "triangle vector: " << triangle_container[count-2].vert11.x << ' '
      //<< triangle_container[count-2].vert22.x << ' '  << triangle_container[count-2].vert33.x << ' ' << endl;
      //cout << "triangle array: " << triangles[count-2].vert11.x << ' '
      //<< triangles[count-2].vert22.x << ' '  << triangles[count-2].vert33.x << ' ' << endl;
      //cout << "count: " << count << endl;
      //cout << "" << endl;
      //triangle_count++;
   }
   //exit (-1);
}

void Scene::copyMaterials () {
   cout << "Scene::copyMaterials" << endl;
   for (int i=0; i<material_container.size(); i++) {
      //if (i==0)
         //cout << "inner loop" << endl;c
      Mtl mtl;
      Material mat = material_container[i];
      mtl.kd.red   = mat.color.red;
      mtl.kd.green = mat.color.green;
      mtl.kd.blue  = mat.color.blue;
      mtl.ks.red   = mat.color.red;
      mtl.ks.green = mat.color.green;
      mtl.ks.blue  = mat.color.blue;
      mtl.ns = 65;
      mtl.reflection = mat.reflection;
      mtl_material_container.push_back (mtl);
   }
/*   cout << "mtl_material_container.size: " << mtl_material_container.size () << endl;
   for (int i=0; i<mtl_material_container.size(); i++)
      cout << "mtl_material_container[" << i << "] colors are: " << mtl_material_container[i].kd.red << ' ' <<
      mtl_material_container[i].kd.green << ' ' << mtl_material_container[i].kd.blue << endl;
   //exit (-1);*/
}

void Scene::generateBBox () {
   //cout << "triangle_container.size(): " << triangle_container.size() << endl;
   float max_x=triangle_container[triangle_count].getVert1().getX(), max_y=triangle_container[triangle_count].getVert1().getY(),
   max_z=triangle_container[triangle_count].getVert1().getZ(), min_x=triangle_container[triangle_count].getVert1().getX(),
   min_y=triangle_container[triangle_count].getVert1().getY(), min_z=triangle_container[triangle_count].getVert1().getZ();

   //cout << "triangle_count, beggining: " << triangle_count << endl;
   for (int i=triangle_count; i<triangle_container.size(); i++) {
      Vec3f vert1 = triangle_container[i].getVert1();
      Vec3f vert2 = triangle_container[i].getVert2_edge1();
      Vec3f vert3 = triangle_container[i].getVert3_edge2();
      //cout << "Vert1: " << vert1.getX() << ' ' << vert1.getY() << ' ' << vert1.getZ() << ' ' << endl;
      //cout << "Vert2: " << vert2.getX() << ' ' << vert2.getY() << ' ' << vert2.getZ() << ' ' << endl;
      //cout << "Vert3: " << vert3.getX() << ' ' << vert3.getY() << ' ' << vert3.getZ() << ' ' << endl;

      /*if ((int)vert3.getX() == (98) || (int)vert2.getX() == (98) || (int)vert1.getX() == (98)) {
         cout << "98" << endl;
         exit (1);
      }*/

      if (vert1.getX() < min_x)
         min_x = vert1.getX();
      else if (vert1.getX() > max_x)
         max_x = vert1.getX();
      if (vert2.getX() < min_x)
         min_x = vert2.getX();
      else if (vert2.getX() > max_x)
         max_x = vert2.getX();
      if (vert3.getX() < min_x)
         min_x = vert3.getX();
      else if (vert3.getX() > max_x)
         max_x = vert3.getX();

      if (vert1.getY() < min_y)
         min_y = vert1.getY();
      else if (vert1.getY() > max_y)
         max_y = vert1.getY();
      if (vert2.getY() < min_y)
         min_y = vert2.getY();
      else if (vert2.getY() > max_y)
         max_y = vert2.getY();
      if (vert3.getY() < min_y)
         min_y = vert3.getX();
      else if (vert3.getY() > max_y)
         max_y = vert3.getY();

      if (vert1.getZ() < min_z)
         min_z = vert1.getY();
      else if (vert1.getZ() > max_z)
         max_z = vert1.getZ();
      if (vert2.getZ() < min_z)
         min_z = vert2.getZ();
      else if (vert2.getZ() > max_z)
         max_z = vert2.getY();
      if (vert3.getZ() < min_z)
         min_z = vert3.getX();
      else if (vert3.getZ() > max_z)
         max_z = vert3.getZ();
    }
    //cout << "max_x: " << max_x << " max_y: " << max_y << " max_z: " << max_z  << endl;
    //cout << "min_x: " << min_x << " min_y: " << min_y << " min_z: " << min_z  << endl;
    vector <Triangle>& tri_vec_ref = triangle_container;
    Bbox bbox (min_x,min_y,min_z,max_x,max_y,max_z,triangle_count,triangle_container.size (),tri_vec_ref);
    Bbox_container.push_back (bbox);
    triangle_count = triangle_container.size ();
    cout << "bbox first triangle: " << bbox.getFirstTri() << endl;
    cout << "bbox latest triangle: " << bbox.getLastTri() << endl;
    //cout << "triangle_count, end: " << triangle_count << endl;
}
