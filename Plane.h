#ifndef __PLANE_h
#define __PLANE_h

#include <iostream>
#include "GeometricObjects.h"
#include "Vec3f.h"

using std::cout;
using std::endl;
using std::ios;

class Plane {
   public:
      Plane (float a, float b, float c, float d, int id);
      bool intersect (Vec3f &camera_dir, Vec3f &camera_ori, const Plane &s, float &t, int x, int y);
      Vec3f getNormal () const { return normal; }
      float getNormalX () const { return normal.x; }
      float getNormalY () const { return normal.y; }
      float getNormalZ () const { return normal.z; }
      float getDist ()  { return dist; }
      int getMaterialId () const { return materialId; }
   private:
      Vec3f normal;
      float dist;
      int materialId;
      int test;
};

#endif
