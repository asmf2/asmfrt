#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

#include "RayTracer.h"

#define DEPTH_RECURSION 4
#define MAX_DIST  2000
#define M_PI 3.141592653589793
#define STANDARD_RADIUS 1000000
#define LIGHT_FALL_OFF 4 * M_PI
#define ALBEDO 0.18

using namespace std;

void RayTracer::draw (Image &image, Scene &scene) {
   cout << "RayTracer::draw" << endl;
   vector <Sphere>& sphere_container = scene.getSphereContainer();
   vector <Plane>& plane_container = scene.getPlaneContainer();
   vector <Triangle>& triangle_container = scene.getTriangleContainer();
   vector <Material>& material_container = scene.getMaterialContainer();
   vector <Mtl>& mtl_material_container = scene.getMtlContainer();
   vector <Light>& light_container = scene.getLightContainer();

   Triangle* tri_arr = scene.getTriArray ();
   const int n_triangles = scene.getNTriangles();

   float inverse_width = 1/(float)image.getWidth();
   float inverse_height = 1/(float)image.getHeight();
   float fov=45;
   float angle = tan (fov/180 * 0.5 * M_PI);
   float aspect_ratio = image.getWidth()/float(image.getHeight());

   Color diffuse_color, specular_color, final_color;

   specular_color.red=0, specular_color.green=0, specular_color.blue=0;

   float xstep = ((inverse_width *2)-1) * angle * aspect_ratio - (-angle * aspect_ratio);
   float px_inicial = -angle * aspect_ratio;
   px_inicial = px_inicial - xstep;
   float ystep = ((inverse_height *2)-1) * angle - (-angle);
   float py_inicial = -angle;
   float py = py_inicial - ystep;

   Vec3f camera_origin = scene.getCamera_ori();
   Vec3f cam_dir = scene.getCamera_ori ();

   Vec3f* dist_ori = new Vec3f [n_triangles];
   Vec3f* s2_ori = new Vec3f [n_triangles];

   for (int i=0; i<n_triangles; i++) {
      dist_ori [i] = camera_origin - tri_arr[i].getVert1();
      s2_ori [i].cross (dist_ori [i],tri_arr [i].getVert2_edge1());
   }

   int ctr=0;

   for (int y=0; y<image.getHeight(); y++) {
      float px = px_inicial;
      py = py + ystep;
      for (int x=0; x<image.getWidth(); x++) {
         diffuse_color.red=0, diffuse_color.green=0, diffuse_color.blue=0;
         float coef = 1.0f;
         int recursion_level = 0;
         px = px + xstep;
         Vec3f camera_dir (px,py,scene.getCameraDirZ());
         //Vec3f camera_ori (0,0,-2000);
         Vec3f camera_ori (cam_dir.getX(),cam_dir.getY(),cam_dir.getZ());
         camera_dir.normalize ();
         do {
            float u,v;
            float t = 3000.0f;
            int object_index=-1, object_type=-1;
            for (unsigned int i=0; i<sphere_container.size(); i++) {
               if (sphere_container[i].intersect (camera_dir,camera_ori,sphere_container[i],t,x,y)) {
                  object_index=i;
                  object_type=1;
               }
            }
            for (unsigned int i=0; i<plane_container.size(); i++) {
               if (plane_container[i].intersect (camera_dir,camera_ori,plane_container[i],t,x,y)) {
                  object_index=i;
                  object_type=2;
               }
            }
            for (unsigned int i=0; i<triangle_container.size(); i++) {
               if (tri_arr[i].intersect (camera_dir,camera_ori,dist_ori[i],s2_ori[i],tri_arr[i],t,u,v,x,y)) {
                  object_index=i;
                  object_type=3;
               }
            }
            if (object_type == -1)
               break;
            Vec3f intersection  (camera_ori + t * camera_dir);
            Mtl mat;
            int mat_type;
            Vec3f normal;
            switch (object_type) {
               case 1:
                  mat_type = sphere_container[object_index].getMaterialId ();
                  mat = mtl_material_container[mat_type];
                  normal = sphere_container[object_index].getNormal (intersection,x,y);
                  break;
               case 2:
                  mat_type = plane_container[object_index].getMaterialId ();
                  mat = mtl_material_container[mat_type];
                  normal = plane_container[object_index].getNormal ();
                  break;
                case 3:
                  mat_type = triangle_container[object_index].getMaterialId ();
                  //mat_type = scene.triangles[object_index].getMaterialId ();
                  mat = mtl_material_container[mat_type];
                  normal = triangle_container[object_index].getNormal (x,y);
                 // normal = scene.triangles[object_index].getNormal (x,y);
            }
            for (unsigned int i=0; i<light_container.size(); i++) {
               Light current = light_container[i];
               Vec3f light_ray_dir (current.pos - intersection);
               light_ray_dir.normalize ();
               if (normal*light_ray_dir <= 0.0f)
                  continue;
               bool in_shadow = false;
               for (unsigned int j=0; j<sphere_container.size(); j++) {
                  if (sphere_container[j].generateShadows (light_ray_dir,intersection,sphere_container[j],x,y)) {
                     in_shadow = true;
                     break;
                  }
                }
               for (unsigned int j=0; j<triangle_container.size(); j++) {
                  if (tri_arr[j].generateShadows (light_ray_dir,intersection,tri_arr[j],t,x,y)) {
                     in_shadow = true;
                     break;
                  }
               }
               if (!in_shadow) {
                  float lambert = (light_ray_dir * normal) * coef;
                  diffuse_color.red += lambert * current.color.red * mat.kd.red;
                  diffuse_color.green += lambert * current.color.green * mat.kd.green;
                  diffuse_color.blue += lambert * current.color.blue * mat.kd.blue;
               }
            }
            coef *= mat.reflection;
            camera_ori = intersection;
            camera_dir = camera_dir - 2 * (camera_dir * normal) * normal;
            recursion_level++;
            //if (currentmar.refraction == ) {
                //Ray r =
         } while ((coef > 0.0f) && (recursion_level < DEPTH_RECURSION));
         int ctr2 = ctr+1;
         int ctr3 = ctr+2;
         a [ctr] = min (diffuse_color.blue*255.0f,255.0f);;
         a [ctr2] = min (diffuse_color.green*255.0f,255.0f);
         a [ctr3] = min (diffuse_color.red*255.0f,255.0f);
         ctr+=3;
      }
   }
   delete dist_ori; delete s2_ori;
}

void RayTracer::draw_multithread2 (Image *image, Scene *scene, int thread_id) {
   cout << "RayTracer::draw_multithread2" << endl;
   vector <Sphere>& sphere_container = scene->getSphereContainer();
   vector <Plane>& plane_container = scene->getPlaneContainer();
   //vector <Triangle>& triangle_container = scene->getTriangleContainer();
   vector <Material>& material_container = scene->getMaterialContainer();
   vector <Mtl>& mtl_material_container = scene->getMtlContainer();
   vector <Light>& light_container = scene->getLightContainer();
   vector <Bbox>& bbox_container = scene->getBboxContainer();

   Triangle* tri_arr = scene->getTriArray ();
   const int n_triangles = scene->getNTriangles();

   const float inverse_width = 1/(float)image->getWidth();
   const float inverse_height = 1/(float)image->getHeight();
   const float fov=scene->getFov();
   const float angle = tan (fov/180 * 0.5 * M_PI);
   const float aspect_ratio = image->getWidth()/float(image->getHeight());

   Color diffuse_color, specular_color, light_intensity, final_color;

   /*float xstep = ((inverse_width *2)-1) * angle * aspect_ratio - (-angle * aspect_ratio);
   float px_inicial = -angle * aspect_ratio;
   px_inicial = px_inicial - xstep;*/
   const float ystep = ((inverse_height *2)-1) * angle - (-angle);
   const float py_inicial = -angle;
   float py = py_inicial - ystep;

   float xx_array [image->getWidth()];

   for (int i=0; i<image->getWidth(); i++)
      xx_array[i] = (((i*inverse_width) *2)-1) * angle * aspect_ratio;

   Vec3f* dist_ori = new Vec3f [n_triangles];
   Vec3f* s2_ori = new Vec3f [n_triangles];

   Vec3f camera_origin = scene->getCamera_ori();
   Vec3f cam_dir = scene->getCamera_ori ();

   for (int i=0; i<n_triangles; i++) {
      dist_ori [i] = camera_origin - tri_arr[i].getVert1();
      s2_ori [i].cross (dist_ori [i],tri_arr[i].getVert2_edge1());
   }

   int ctr=0;
   int initial_x=0;
   if (thread_id==1) {
      //return;
      initial_x=1;
      ctr=3;
   }

   for (int y=0; y<image->getHeight(); y++) {
      //float px = px_inicial;
      py = py + ystep;
      //cout << y << endl;
      for (int x=initial_x; x<image->getWidth(); x+=n_threads) {
         diffuse_color.red=0, diffuse_color.green=0, diffuse_color.blue=0;
         specular_color.red=0, specular_color.green=0, specular_color.blue=0;
         //float coef = 1.0f;
         int recursion_level = 0;
         //px = px + xstep;
         //float xx = (((x*inverse_width) *2)-1) * angle * aspect_ratio;
         Vec3f camera_dir (xx_array[x],py,scene->getCameraDirZ());
         Vec3f camera_ori (cam_dir.getX(),cam_dir.getY(),cam_dir.getZ());

         camera_dir.normalize ();

         do {
            float u=0,v=0;
            float t = 3000.0f;
            int object_index=-1;/*, object_type=-1;


            for (int i=0; i<sphere_container.size(); i++)
               if (sphere_container[i].intersect (camera_dir,camera_ori,sphere_container[i],t,x,y)) {
                  object_index=i;
                  object_type=1;
               }
            for (int i=0; i<plane_container.size(); i++)
               if (plane_container[i].intersect (camera_dir,camera_ori,plane_container[i],t,x,y)) {
                  object_index=i;
                  object_type=2;
               }*/
            for (int j=0; j<bbox_container.size(); j++)
               if (bbox_container[j].intersect (camera_dir,camera_ori))
                  for (int i=bbox_container[j].getFirstTri(); i<bbox_container[j].getLastTri(); i++)
                  //for (int i=0; i<n_triangles; i++)
                     //if (triangle_container[i].intersect (camera_dir,camera_ori,triangle_container[i],t,x,y)) {
                     //if (tri_arr[i].intersect (camera_dir,camera_ori,tri_arr [i],t,x,y)) {
                     if (tri_arr[i].intersect (camera_dir,camera_ori,/*edge1[i],edge2[i],*/dist_ori[i],s2_ori[i],tri_arr[i],t,u,v,x,y))
                        object_index=i;
                        //object_type=3;

            //cout << "u & v: " << u << ' ' << v << endl;
            //exit (-1);
            //if (object_type == -1)
            if (object_index == -1)
               break;
            Vec3f intersection  (camera_ori + t * camera_dir);

            Mtl mat;
            int mat_type;
            Vec3f normal;
            Vec3f vert1 = tri_arr[object_index].getVert1 ();
            Vec3f vert2 = tri_arr[object_index].getVert2_edge1 () + vert1;
            Vec3f vert3 = tri_arr[object_index].getVert3_edge2() + vert1;
            Vec3f hit_normal = (1 - u - v) * vert1 + u * vert2 + v * vert3;
            hit_normal.normalize ();
            /*switch (object_type) {
               case 1:
                  mat_type = sphere_container[object_index].getMaterialId ();
                  mat = mtl_material_container[mat_type];
                  normal = sphere_container[object_index].getNormal (intersection,x,y);
                  break;
               case 2:
                  mat_type = plane_container[object_index].getMaterialId ();
                  mat = mtl_material_container[mat_type];
                  normal = plane_container[object_index].getNormal2 ();
                  break;
               case 3:*/
                  //mat_type = triangle_container[object_index].getMaterialId ();
                  mat_type = tri_arr [object_index].getMaterialId ();
                  mat = mtl_material_container[mat_type];
                  //normal = triangle_container[object_index].getNormal (x,y);
                  normal = tri_arr [object_index].getNormal (x,y);
            //}
            for (unsigned int i=0; i<light_container.size(); i++) {
               Light current = light_container[i];
               //cout << "current: " << current.color.red << ' ' << current.color.green << ' ' << current.color.blue << ' ' << endl;
               Vec3f light_ray_dir (current.pos - intersection);
               float squared_length = light_ray_dir.normalize_return_squared_lenght ();
               if (normal*light_ray_dir <= 0.0f)
                  continue;
               bool in_shadow = false;

               for (int z=0; z<bbox_container.size(); z++)
                  if (bbox_container[z].intersect(light_ray_dir,intersection))
                     for (int j=bbox_container[z].getFirstTri(); j<bbox_container[z].getLastTri(); j++)
                        if (tri_arr[j].generateShadows (light_ray_dir,intersection,/*edge1[j],edge2[j],*/tri_arr[j],t,x,y)) {
                           in_shadow = true;
                           break;
                        }

               if (!in_shadow) {
                  const float lambert = light_ray_dir * normal;
                  const float attenuation = LIGHT_FALL_OFF * squared_length;
                  light_intensity = current.color / attenuation;

                  diffuse_color += ALBEDO * light_intensity * lambert;

                  Vec3f reflected = light_ray_dir - 2 * (light_ray_dir * hit_normal) * hit_normal;

                  reflected.normalize();

                  specular_color += light_intensity * std::pow (std::max(0.f,reflected*camera_dir),mat.ns);

                  /*if (x==200 && y==200) {
                     cout << "x * y: " << x << ' ' << y << endl;
                     cout << "hit_normal: " << hit_normal.x << ' ' << hit_normal.y << ' ' << hit_normal.z << ' ' << endl;
                     cout << "Vert1: " << vert1.x << ' ' << vert1.y << ' ' << vert1.z << ' ' << endl;
                     cout << "Vert2: " << vert2.x << ' ' << vert2.y << ' ' << vert2.z << ' ' << endl;
                     cout << "Vert3: " << vert3.x << ' ' << vert3.y << ' ' << vert3.z << ' ' << endl;
                     cout << "" << endl;
                  }
                  if (x==50 && y==50) {
                     cout << "x * y: " << x << ' ' << y << endl;
                     cout << "hit_normal: " << hit_normal.x << ' ' << hit_normal.y << ' ' << hit_normal.z << ' ' << endl;
                     cout << "Vert1: " << vert1.x << ' ' << vert1.y << ' ' << vert1.z << ' ' << endl;
                     cout << "Vert2: " << vert2.x << ' ' << vert2.y << ' ' << vert2.z << ' ' << endl;
                     cout << "Vert3: " << vert3.x << ' ' << vert3.y << ' ' << vert3.z << ' ' << endl;
                     cout << "" << endl;
                  }*/
               }
            }


            final_color = diffuse_color * mat.kd + specular_color * mat.ks;

            /*if (x==50 && y==50)
                cout << "final_color in x and y 50 " << x << ' ' << y <<  ' ' << final_color.red  << ' ' << final_color.green << ' ' << final_color.blue << endl;

            if (x==200 && y==200)
                cout << "final_color in x and y 200 " << x << ' ' << y <<  ' ' << final_color.red  << ' ' << final_color.green << ' ' << final_color.blue << endl;
*/
            camera_ori = intersection;
            camera_dir = camera_dir - 2 * (camera_dir * normal) * normal;
            recursion_level++;
            //if (currentmar.refraction == ) {
                //Ray r =
         } while (/*(coef > 0.0f) && */(recursion_level < DEPTH_RECURSION));
         a [ctr]   = min (final_color.blue*255.0f,255.0f);
         a [ctr+1] = min (final_color.green*255.0f,255.0f);
         a [ctr+2] = min (final_color.red*255.0f,255.0f);
         ctr+=6;
      }
   }
   pthread_exit (NULL);
   delete dist_ori; delete s2_ori;
}
