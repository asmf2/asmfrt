//#include <QApplication>
//#include <QFont>
//#include <QPushButton>

#include <iostream>
#include "PerformanceManager.h"
#include "InputHandler.h"
#include "TGAManager.h"
#include "TextManager.h"
#include "RayTracer.h"
#include <pthread.h>
#include <cstdlib>
#include <cmath>

using std::cout;
using std::endl;
using std::ios;

int main (int argc, char* argv[]) {
  InputHandler ih (argc);
   if (!ih.checkNumberArguments ())
      return 0;
   if (!ih.checkEntries (argv))
      return 0;
   ih.swapEntries (argv);
   PerformanceManager pf;
   pf.start ();
   Image image;
   Scene scene (0,0);
   int n_threads;
   {
      TextManager textManager (argv[1],false);
      if (textManager.isFileOpen()) {
         textManager.storeFilename (argv[1]);
         textManager.scanFile (image, scene,0);
         textManager.closeFile(0);
         n_threads = textManager.getNThreads();
         scene.generateBBox ();
      }
      if (argc==3) {
         textManager.openFile (argv[2]);
         if (textManager.isFileOpen()) {
            textManager.storeFilename (argv[2]);
            textManager.scanObjFile (image, scene);
            textManager.closeFile(0);
            n_threads = textManager.getNThreads();
            scene.generateBBox ();
         }
      }
      if (argc==4) {
         textManager.openFile (argv[2]);
         if (textManager.isFileOpen()) {
            textManager.storeFilename (argv[2]);
            textManager.scanFile (image, scene,1);
            textManager.closeFile(0);
            n_threads = textManager.getNThreads();
         }
         textManager.openFile (argv[3]);
         if (textManager.isFileOpen()) {
            textManager.storeFilename (argv[3]);
            textManager.scanObjFile (image, scene);
            textManager.closeFile(0);
            n_threads = textManager.getNThreads();
            scene.generateBBox ();
         }
      }
   }
   pf.end();
   pf.print();
   scene.copyMaterials ();
   if (n_threads==1) {
      //vector <Triangle>().swap (scene.triangle_container);
      scene.toWindCW();
      pf.start ();
      TGAManager tgaManager ("z.tga",true);
      if (tgaManager.isFileOpen()) {
         scene.setTriMem ();
         scene.copyTriangles ();
         tgaManager.writeHeadersData (image);
         RayTracer ray_tracer (image.getSize(),n_threads);
         ray_tracer.draw (image, scene);
         tgaManager.writeImage ((char *)ray_tracer.getImgPtr(),image.getSize());
         tgaManager.closeFile(true);
         scene.unsetTriMem ();
         ray_tracer.deleteImgPtr ();
      }
      pf.end();
      pf.print();
   }
   else {
      pf.start ();
      scene.setTriMem ();
      scene.copyTriangles ();
      vector <Triangle>().swap (scene.getTriangleContainer());
      scene.toWindCW();
      pthread_t threads [n_threads];
      thread_data td_array [n_threads];
      void *status;
      TGAManager tgaManager ("z.tga",true);
      if (tgaManager.isFileOpen()) {
         tgaManager.writeHeadersData (image);
         RayTracer rt (image.getSize(),n_threads);
         int rc;
         for (int i=0; i<n_threads; i++) {
            //cout << "main() : creating thread, " << i << endl;
            td_array[i].thread_id=i;
            td_array[i].rt_ptr = &rt;
            td_array[i].img_ptr = &image;
            td_array[i].scene_ptr = &scene;
            //cout << "td_array.thread_index: " << td_array[i].thread_id << endl;
            rc = pthread_create (&threads[i], NULL, RayTracer::run_thread, &td_array[i]);
         }
         if (rc) {
            cout << "Error:unable to create thread," << rc << endl;
            exit(-1);
         }
         for (int i=0; i<n_threads; i++ ) {
            rc = pthread_join(threads[i], &status);
            if (rc) {
               cout << "Error:unable to join," << rc << endl;
               exit(-1);
            }
         }
         tgaManager.writeImage ((char *)rt.getImgPtr(),image.getSize());
         tgaManager.closeFile(1);
         scene.unsetTriMem ();
         rt.deleteImgPtr ();
      }
      pf.end ();
      pf.print();
   }
   /*QApplication app(argc, argv);

   QPushButton quit("Quit");

   quit.resize(750, 300);
   quit.setFont(QFont("Times", 16, QFont::Bold));

   QObject::connect(&quit, SIGNAL(clicked()), &app, SLOT(quit()));

   quit.show();

   return app.exec();*/
}
