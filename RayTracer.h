#ifndef __RAYTRACER_h
#define __RAYTRACER_h

#include "Sphere.h"
#include "Image.h"
#include "Scene.h"
#include "Vec3f.h"
#include "PerformanceManager.h"

class RayTracer;

struct thread_data {
   int thread_id;
   RayTracer* rt_ptr;
   Image* img_ptr;
   Scene* scene_ptr;
};

class RayTracer {
   public:
      RayTracer (int size, int nthreads) {
         a = new unsigned char [size];
         n_threads = nthreads;
         //cout << "n_threads it is: " << n_threads << endl;
      }
      void draw (Image &image, Scene &scene);
      void draw_multithread2 (Image *image, Scene *scene, int thread_id);
      unsigned char * getImgPtr () {return a;}
      void deleteImgPtr () { delete a; }
      static void* run_thread (void* ptr) {
         cout << "run_thread..." << endl;
         void *nullptr;
         thread_data* td = static_cast <thread_data*> (ptr);
         td->rt_ptr->draw_multithread2 (td->img_ptr,td->scene_ptr,td->thread_id);
         return nullptr;
         cout << "" << endl;
      }
   private:
      unsigned char *a;
      int n_threads;
};


#endif
