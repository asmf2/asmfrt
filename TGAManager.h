#ifndef __TGAMANAGER_h
#define __TGAMANAGER_h

#include "FileManager.h"
#include "Image.h"

class TGAManager : public FileManager
{
    public:
        TGAManager (const char *,bool);
        bool checkFileRead ();
        bool checkHeaderData();
        bool extractImageFeatures();
        int  getBpp() { return bpp; }
        int  getHeight() { return height; }
        int  getWidth() { return width; }
        void readHeaderInformation();
        void readTgaData (char * mem_adress, int img_size) { file_reader.read (mem_adress, img_size ); }
        void writeHeadersData (Image &);
        void writeImage (char * img_arr, int img_size) {file_writer.write (img_arr, img_size); }
    private:
        char TGAheader[12];
        char TGAcompare[12];
        char header[6];
        int  height;
        int  width;
        int  bpp;
};

#endif
