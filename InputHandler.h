#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>

using std::vector;
using std::string;

class InputHandler {
public:
  InputHandler (int argc) { nArgs = argc, n_txt=0, n_mtl=0, n_obj=0; }
  bool checkEntries (char* []);
  bool checkNumberArguments ();
  void swapEntries (char* []);
private:
  int nArgs;
  int n_txt, n_mtl, n_obj;
  vector <string> file_types;
};
