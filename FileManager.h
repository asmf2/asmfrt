#include <iostream>
#include <fstream>

#ifndef FILEMANAGER_H
#define FILEMANAGER_H

using namespace std;

class FileManager {
public:
    FileManager (const char *, bool);
    bool isFileOpen ();
    void closeFile (bool);
    void openFile (const char * argv2) {file_reader.open (argv2); }
    void storeFilename (const char * argv) { file_name = argv; }
protected:
    ofstream file_writer;
    ifstream file_reader;
    int getPtrPosition;
    string file_name;
};

#endif
