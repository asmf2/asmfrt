#include <iostream>
#include "TGAManager.h"

using std::ios;
using std::cout;
using std::endl;

TGAManager::TGAManager(const char * filename, bool choice) : FileManager (filename, choice) {
    TGAheader[0] = 0; TGAheader[1] = 0; TGAheader[2] = 2; TGAheader[3] = 0; TGAheader[4] = 0; TGAheader[5] = 0; TGAheader[6] = 0;
    TGAheader[7] = 0; TGAheader[8] = 0; TGAheader[9] = 0; TGAheader[10] = 0; TGAheader[11] = 0;
}

void TGAManager::readHeaderInformation() {
  file_reader.read (TGAcompare,sizeof(TGAcompare));
  file_reader.read (header,sizeof(header));
}

bool TGAManager::checkFileRead() {
  if (file_reader.eof () == 1) {
    cout << "The eof bit was set while trying to read the TGA file. Please try run the program again with a different TGA file: " << endl;
    return false;
  }
  else if (file_reader.bad () == 1) {
    cout << "Badbit set. A likely loss of integrity of the stream happened. Please try to run with another TGA file: " << endl;
    return false;
  }
  else if ((file_reader.fail () == 1) && (file_reader.bad () == 0)) {
    cout << "FailBit set. An error likely related to the internal logic of the operation happened. Please try to run the program again: " << endl;
    return false;
  }
  else
    return true;
}

bool TGAManager::checkHeaderData() {
  for (unsigned int i=0; i<sizeof (TGAcompare); i++) {
    if (TGAcompare[i] != TGAheader[i]) {
      cout << "The header file is corrupted in the: " << i << " byte of the file or it is not a valid tga file. Please try running the program again" << endl;
      return false;
    }
  }
  return true;
}

bool TGAManager::extractImageFeatures() {
  width = header[1] * 256 + header[0];
  height = header[3] * 256 + header[2];
  bpp = header[4];
  if (width <= 0 || height <= 0  || (header[4] != 24 && header[4] != 32)) {
    cout << "The data about the file width, height or bpp is incorrect. Please try to load a different tga file: " << endl;
    return false;
  }
  return true;
}

void TGAManager::writeHeadersData (Image &image) {
    file_writer.put(0).put(0);
    file_writer.put(2);
    file_writer.put(0).put(0);
    file_writer.put(0).put(0);
    file_writer.put(0);
    file_writer.put(0).put(0);
    file_writer.put(0).put(0);

    file_writer.put((unsigned char)(image.getWidth() & 0x00FF)).put((unsigned char)((image.getWidth() & 0xFF00) / 256));
    file_writer.put((unsigned char)(image.getHeight() & 0x00FF)).put((unsigned char)((image.getHeight() & 0xFF00) / 256));
    file_writer.put(24);
    file_writer.put(0);
}
