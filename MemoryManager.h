#include "Image.h"

#ifndef MEMORYMANAGER_H_INCLUDED
#define MEMORYMANAGER_H_INCLUDED

class MemoryManager {
  public:
    bool setTextureMemory (const Image &image);
    char * getMemoryAdress ();
    void freeMemory();
  private:
    char * textureStorage;
};

#endif // MEMORYMANAGER_H_INCLUDED
